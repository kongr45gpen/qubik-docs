# QUBIK mission documentation #

This repository contains the source code of QUBIK mission documentation.

The rendered documentation can be found [here](https://qubik.libre.space/).

## Styling guide ##

reStructeredText in this repository shall follow this styling guide for source code consistency.
These rules are **not** checked as part of quality gating with a CI job.


### Sections ###

The succession of headings shall follow the [Python Developer's Guide for documentation](https://devguide.python.org/documentation/markup/#sections):

  * 1st level, `#` with overline
  * 2nd level, `*` with overline
  * 3rd level, `=`
  * 4th level, `-`
  * 5th level, `^`
  * 6th level, `"`


### Lists ###

Numbered lists shall be auto-numbered using the `#.` sign, unless required otherwise.
Unnumbered lists shall only use `*` sign, unless required otherwise.


### Paragraphs ###

All sentences shall end with a punctuation mark followed by a newline character.
Lines shall be wrapped to 79 characters (GNU formatting style).


## License ##

[![license](https://img.shields.io/badge/license-CC%20BY--SA%204.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202020-Libre%20Space%20Foundation-6672D8.svg)](https://libre.space/)
