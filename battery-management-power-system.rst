Battery Management Power System
###############################
* Description: https://gitlab.com/librespacefoundation/qubik/qubik-bmps/-/issues/11
* Repository
* Releases

.. figure:: img/kill-switch.png
   :alt: Kill Switch Architecture
   :align: center

   Kill Switch Architecture.

System Perfomance
*****************
Related issues:

* https://gitlab.com/librespacefoundation/qubik/qubik-bmps/-/issues/21
* https://gitlab.com/librespacefoundation/qubik/qubik-bmps/-/issues/26
* https://gitlab.com/librespacefoundation/qubik/qubik-bmps/-/issues/20

System Assembly
***************
Related issue:

* https://gitlab.com/librespacefoundation/qubik/qubik-docs/-/issues/13
* Before Gluing and coating a :doc:`dry fit <pre-assembly>` must be done

System Testing
**************
Related documentation:

* https://gitlab.com/librespacefoundation/qubik/qubik-bmps/-/wikis/home
