Budget
******

Qubik RCS
=========

Check the related issue https://gitlab.com/librespacefoundation/qubik/qubik-org/-/issues/18
for results.

Link Budget - Uplink
====================

.. figure:: img/qubik-uplink.png
   :alt: qubik-uplink

   Qubik uplink budget

+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| **Transmitter  - Parameters**     |                   | **Comment**                                                                                                                               |
+===================================+===================+===========================================================================================================================================+
| Transmitter Frequency (MHz)       | 435.24            | IARU Defined                                                                                                                              |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Transmitter Power (dBm)           | 38.00             | https://enigma-shop.com/index.php?option=com_hikashop&ctrl=product&task=show&name=mitsubishi-ra07h4047m-rf-power-amplifier-module&cid=753 |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Transmitter Power (dBW)           | 8.00              | Transmitter Power (dBm)-30                                                                                                                |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Antenna circuit loss(RFDN) (dB)   | -2.00             | TBD                                                                                                                                       |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Antenna gain (dBi)                | 14.95             | Wimo X-Quad UHF                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| D3dB antenna (deg)                | 36.00             | https://granasat.ugr.es/2013/11/ground-station-equipment/                                                                                 |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Pointing accuracy (deg)           | 1.00              |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| EIRP(dBW)                         | 20.95             | Transmitter Power (dBW)+RFDN (dB)+Antenna gain (dBi)                                                                                      |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| EIRP(dBm)                         | 50.95             | Transmitter Power (dBm)+RFDN (dB)+Antenna gain (dBi)                                                                                      |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| **Path – Parameters**             |                   |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Elevetion angle (deg)             | 6.96872           |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Altitude (km)                     |                   |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Slant Range (km)                  | 1513.69           |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Free Space Loss (dB)              | -148.87           | gr-leo calculations                                                                                                                       |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Atmospheric/Ionospheric Loss (dB) | -1.58             | gr-leo calculations                                                                                                                       |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Rainfall Loss (dB)                | -0.003            | gr-leo calculations                                                                                                                       |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Total Path Loss (dB)              | -150.45           |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| **Receiver – Parameters**         |                   |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Polarization loss (dB)            | -4.00             | Assumption                                                                                                                                |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Pointing loss (dB)                | -3.00             | From IARU link budget, for Theta2 = 60deg                                                                                                 |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| D3dB antenna (deg)                | Omnidirectional   | Dipole Lamda/2                                                                                                                            |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Pointing accuracy (deg)           | 0.00              |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Antenna circuit loss(RFDN) (dB)   | -1.04             | Connectors, BalUn, cable                                                                                                                  |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Antenna gain (dBi)                | 2.15              | Dipole Lamda/2                                                                                                                            |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Total Antenna Gain(dB)            | -5.89             | Polarization loss (dB) +Pointing Loss(dB)+RFDN(dB)+Antenna Gain(dBi)                                                                      |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Antenna Noise Temp (K)            | 290.00            | It is constant 290k for satellite                                                                                                         |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Received Signal (dBm)             | -105.39           | EIRP(dBm) + Total Path Loss (dB) + Total Antenna Gain(dB)                                                                                 |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| **Receiver – Performance**        |                   |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Front-End NF(dB)                  | 7.00              | Assumption from datasheet related with sensitivity for FSK w/o FEC                                                                        |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Front-End Noise Temp (K) at 290K  | 1163.44           | 290(k)*(10^(NF(dB)/10)-1)                                                                                                                 |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Tsys(K)                           | 1453.44           | Antenna Noise Temp (K) + Front-End Noise Temp (K)                                                                                         |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Tsys(dBK)                         | 31.62             | 10*LOG10(Tsys)                                                                                                                            |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| G/T(dB/K)                         | -37.51            | Total Antenna Gain(dB)-Tsys(dBK)                                                                                                          |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Noise Floor (dBm/Hz)              | -166.98           | 10*LOG10(k*1000*Tsys(K))                                                                                                                  |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| MDS(dBm)                          | -114.571736674172 | Noise Floor(dBm) + 10*LOG10(Required BW(Hz))                                                                                              |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
|                                   |                   |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Symbol rate (samples/s)           | 9600.00           | 1200-9600                                                                                                                                 |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Channel symbol rate (dBHz)        | 39.82             | 10*LOG10(Symbol Rate(samples/s))                                                                                                          |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Implementetion Loss (dB)          | -1.00             |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Received SNR (dB)                 | 19.18             | Received Signal (dBm)-(Noise Floor(dBm/Hz)+10*LOG10(Required BW(Hz)))                                                                     |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Required SNR (dB)                 | 10.00             |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Received Es/N0(dB)                | 20.76             | EIRP(dBW)+Total Path Loss(dB)+G/T(dB/K)+Impl. Loss(dB)-[k(dBW/(K Hz))+Symbol Rate(dBHz)]                                                  |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| RF Carrier Modulation, Type       | FSK               |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| RF Carrier Modulation, Format     | NRZ-M             |                                                                                                                                           |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| User Bit Rate, b/s                | 7200.00           | Symbol Rate(samples/s)*Coding Rate*LOG2(Symbol M-arity)                                                                                   |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Bit Error Rate                    | 10^-5             | ECSS-E-HB-50A                                                                                                                             |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Data Coding, Type                 | RS(255,223)       | We need for RS(255,223) concatenated or RS(128,96)                                                                                        |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Required Bandwidth (Hz)           | 17400.00          | Set by AX5043                                                                                                                             |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Symbols M-arity                   | 2.00              | Due to FSK                                                                                                                                |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Coding rate                       | 0.75              | For FSK and RS(255,223)                                                                                                                   |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Received Eb/N0(dB)                | 22.01             | Es/N0(Rx) -10*LOG10(LOG2(M))-10*LOG10(Coding Rate)                                                                                        |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Required Eb/N0(dB)                | 7.50              | Minus 2.5 from unencoded FSK-2                                                                                                            |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Margin (dB)                       | 14.51             | Received Eb/N0(dB)-Required Eb/N0(dB)                                                                                                     |
+-----------------------------------+-------------------+-------------------------------------------------------------------------------------------------------------------------------------------+

`Spreadsheet calculator <https://cloud.libre.space/s/TLjHw23Szr9z3s7>`__
(it is better to download it)

Link Budget - Downlink
======================

**TBD**

Energy Harvesting Power Budget
==============================

By using a `python script <https://gitlab.com/librespacefoundation/qubik/qubik-org/-/snippets/1896694>`__
to calculate the power coefficient and finally the power production of a
satellite.

**Inputs:**

.. code:: python

   # Put TLE
   line1 = ('1 84001U          20001.00000000  .00000000  00000-0  50000-4 0 08')
   line2 = ('2 84001  97.0000 156.0000 0001497   0.0000 124.0000 15.90816786 02')
   satellite = twoline2rv(line1, line2, wgs72)
   # Set the start day for simulation, Julian day
   JD_ini = jday(2020, 4, 15, 8, 20, 0)
   # Initialize parameters
   total_time = 400  # in min
   # Initialize angular velocities in rad/min
   w_body = np.array([30.0, 10.0, 25.0])
   # Initial angle conditions in deg
   angle_body_curr = np.deg2rad([0.0, 0.0, 0.0])
   # PV efficient in each side [xp, xm, yp, ym, zp, zm]
   pv_eff = np.array([0.25, 0.25, 0.25, 0.25, 0.25, 0.25])
   # Number of PV in each side [xp, xm, yp, ym, zp, zm]
   pv_num = np.array([3.0, 3.0, 3.0, 3.0, 3.0, 3.0])
   # Active area of each PV in mm^2, e.g. 45mmx15mm
   pv_area = np.array([(45.0*15.0), (45.0*15.0), (45.0*15.0), (45.0*15.0),
                       (45.0*15.0), (45.0*15.0)])
   # Solar irradiance (kW/m^2) in a specific orbit
   si = 1.4

Note: solar cell is http://ixapps.ixys.com/DataSheet/SM141K04LV.pdf

**Results:**

#. Mean Power coefficient of each side, [Xp_m, Xm_m, Yp_m, Ym_m, Zp_m, Zm_m]: [0.16756193891221607, 0.17041088209246041, 0.19111270153573892, 0.19375282007969838, 0.15851510079365622, 0.15893245086986432]
#. Total Mean Power coefficient, 1.0402858942836344
#. Mean Power of each side in mW, [Xp_m, Xm_m, Yp_m, Ym_m, Zp_m, Zm_m]: [118.7595242 , 120.77871268, 135.45112721, 137.32231123, 112.34757769, 112.64337455]
#. Total Mean Power in mW, 737.3026275735258

For 400min or ~4 orbits

.. figure:: img/qubik-SM141K04LV.png
   :alt: qubik-SM141K04LV

   Qubik Power Budget


Power Budget
============

**TBD**

Data Budget
===========

**TBD**

Mass Budget
===========

**TBD**

Pointing Budget
===============

Not applicable to Qubik satellite due to missing determination and control attitude system.
