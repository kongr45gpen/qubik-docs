Cabling
#######
* Description
* Repository: a wireviz source file can be placed in `this directory <https://gitlab.com/librespacefoundation/qubik/qubik-docs/-/tree/master/contrib?ref_type=heads>`__
* Releases

System Perfomance
*****************
.. warning:: Neither PVC bulk materials nor PVC plastic films shall be used in space applications, according to ECSS-Q-70-71A.

.. note:: Some wires like VBAT+ and VBAT- must be twisted. With the exception of the solar array, power lines shall be such that each line is twisted with its return, when the structure is not used as a return, as refered ECSS‐E‐ST‐20C.

System Assembly
***************

.. note:: Follow the `NASA workmanship guide <https://workmanship.nasa.gov/lib/insp/2%20books/frameset.html>`__ for cable lacing. Apply this in Main harness and COMMS Programmer cable. Use cyanoacrylic glue to solidify the knots in cable.

.. note:: Follow ECSS‐Q‐ST‐70‐08C for cabling assembly.

.. figure:: img/cable-all.png
   :alt: cable-all

   Qubik harnessing

Battery cable
==============

* Length:(the exact length must be tuned in assembly process)
* Type: 26AWG
* VBAT+: red
* VBAT-: black
* TBAT: yellow
* Connector: DF11-6DS-2C

   *  pin1-2: VBAT+ (Length 45mm)
   *  pin3-4: VBAT- (Length 55mm)
   *  pin5: COMMON wich is same with VBAT- (Length 60mm)
   *  pin6: TBAT (Length 55mm)
   *  `Connector assembly - DF11-2428SC <https://www.hirose.com/product/document?clcode=CL0550-0197-4-00&productname=DF11-TA2428HC&series=DF11&documenttype=ToolInstructionManual&lang=en&documentid=D31583_en>`__

.. figure:: img/battery-cable.png
   :alt: battery-cable

   Battery Cable

.. note:: As a convention we name (A) the battery attached on the BPMS board and (B) the other one.

*  pin1: (A) VBAT+
*  pin2: (B) VBAT+
*  pin3: (A) VBAT-
*  pin4: (B) VBAT-
*  pin5: (B) COMMON (It could be also (A) but it is easier to solder on (B))
*  pin6: (B) TBAT (It is preferable to measure the colder battery as cold is the more harsh condition for the battery performance)

.. figure:: img/battery-stack.png
   :alt: Battery-Stack

   Battery Stack

Antenna cable
=============

*  Length: 60mm
*  `U.FL-2LPHF6-066N1-A-60 <https://www.hirose.com/product/p/CL0321-5193-9-48>`__

.. figure:: img/antenna-cable.png
   :alt: antenna-cable

   Antenna Cable

Main harness
============

*  KILL-GND: black 26AWG, length: 80mm
*  GND: blue 26AWG, length: 80mm
*  VBAT+: red 26AWG, length: 80mm
*  EPS-PWR: yellow 26AWG, length: 80mm
*  GND (TOP-PV-): black 28AWG, length: 35mm
*  VBAT+ (TOP-PV+): red 28AWG, length: 35mm
*  COMMS-GND: black 28AWG, length: 50mm
*  ANT-REL: red 28AWG, length: 50mm
*  VBAT-SENSE: red 28AWG, length: 50mm
*  ANT-SENSE: red 28AWG, length: 50mm
*  Connectors:

   *  DF11-6DS-2C:

      *  pin1: KILL-GND
      *  pin2: GND
      *  pin3: VBAT+
      *  pin4: EPS-PWR
      *  pin5: GND (TOP-PV-)
      *  pin6: VBAT+ (TOP-PV+)
      *  `Connector assembly - DF11-2428SC <https://www.hirose.com/product/document?clcode=CL0550-0197-4-00&productname=DF11-TA2428HC&series=DF11&documenttype=ToolInstructionManual&lang=en&documentid=D31583_en>`__

   *  `JST SH Connector Female 2-Pin 1.0mm <http://grobotronics.com/images/datasheets/A1001H-XP-1---B5.pdf>`__

      *  pin1: VBAT+
      *  pin2: GND
      *  `Connector assembly <http://www.jst-mfg.com/product/pdf/eng/handling_e.pdf>`__

   *  DF11-8DS-2C

      *  pin1: VBAT+
      *  pin2: EPS-PWR
      *  pin3: KILL-GND
      *  pin4: GND
      *  pin5: COMMS-GND (GND that no connected to in base plate GND)
      *  pin6: ANT-REL
      *  pin7: VBAT-SENSE
      *  pin8: ANT-SENSE
      *  `Connector assembly - DF11-2428SC <https://www.hirose.com/product/document?clcode=CL0550-0197-4-00&productname=DF11-TA2428HC&series=DF11&documenttype=ToolInstructionManual&lang=en&documentid=D31583_en>`__

   *  `PicoBlade Female-to-PicoBlade Female 151340600 <https://www.molex.com/molex/products/part-detail/cable_assemblies/0151340600>`__

      *  pin1: VBAT-SENSE
      *  pin2: ANT-SENSE
      *  pin3: ANT-REL
      *  pin4: COMMS-GND (GND that no connected to in base-plate GND)
      
      .. note:: we need to cut one side and we put `DF11-2428SCFA(04) <https://www.hirose.com/en/product/p/CL0543-0550-0-04>`__

.. figure:: img/main-cable.png
   :alt: main-cable

   Main Harness

COMMS Programmer cable
======================

*  Length: 50mm
*  1-pin is connected to 1-pin to other connector, so on one side we put new `1.25mm Pitch, PicoBlade Receptacle Crimp Housing <https://www.molex.com/molex/products/part-detail/crimp_housings/0510210600>`__.
*  `PicoBlade Female-to-PicoBlade Female 151340600 <https://www.molex.com/molex/products/part-detail/cable_assemblies/0151340600>`__

.. figure:: img/comms-jtag.png
   :alt: comms-jtag

   COMMS Programming Cable

Umbilical
=========

*  Length: In depends on the programmer.
*  pin1: VBAT-, black 26 AWG, when the switches is pushed, RBF in QUBIK
*  pin2: EPS-PWR, yellow 26 AWG, when the switches is pushed, RBF in QUBIK
*  pin3: GND, blue 26 AWG
*  pin4: VBAT+, red 26AWG
*  pin5: JTDO (COMMS), 28AWG red
*  pin6: NC
*  pin7: NRST (COMMS), 28AWG red
*  pin8: NC
*  pin9: SWDIO (COMMS), 28AWG red
*  pin10: NC
*  pin11: GND (COMMS), 28AWG black
*  pin12: NC
*  pin13: SWCLK (COMMS), 28AWG red
*  pin14: NC
*  pin15: VCCQ (COMMS), 28AWG red
*  pin16: NC
*  `Connector assembly <https://gr.mouser.com/datasheet/2/185/DF11_CL0543-0658-7-05_2d-1611074.pdf>`__

System Testing
**************
Related documentation:

* TBD
