=============================================
Environmental and Functional testing
=============================================

Vibration test
==============
Perform Resonance survey, Sine, Quasi-static and Random vibration on the Satellite as specified in ECSS-E-ST-10-03C.
Equipment for testing:

#. Shaker table
#. Adapter plate to mount DUT on shaker table in X, Y and Z configuration

Test profiles for vibration test
================================
The Satellite must be tested at qualification levels following ECSS-E-ST-10-03C standards with profiles shown in the Tables below:

+--------------------------+
| Resonance survey profile |
+===========+==============+
| Frequency | Level (g)    |
+-----------+--------------+
| 5 - 2000  | 0.4          |
+-----------+--------------+

+----------------------------------------------+
| Sine Vibration                               |
+===========+===========+======================+
| Frequency | Level (g) | Sweep Rate (oct/min) |
+-----------+-----------+----------------------+
| 5 - 100   | 2.5       | 2                    |
+-----------+-----------+----------------------+
| 100 - 125 | 1.25      | 2                    |
+-----------+-----------+----------------------+

+----------------------------------------------+
| Quasi Static                                 |
+===========+===========+======================+
| Frequency | Level (g) | Full Cycles          |
+-----------+-----------+----------------------+
| 50        | 9.6       | 6                    |
+-----------+-----------+----------------------+

+------------------------------------------------------------------+
| Random Vibration                                                 |
+=============+===================+=================+==============+
| Frequency   | Level (g2/Hz)     | Overall (g rms) | Test Time (s)|
+-------------+-------------------+-----------------+--------------+
| 20          | 0.006             | 10              | 120          |
+-------------+-------------------+-----------------+--------------+
| 20 - 120    | 0.006 + 6 dB/oct  | 10              | 120          |
+-------------+-------------------+-----------------+--------------+
| 100 - 700   | 0.04              | 10              | 120          |
+-------------+-------------------+-----------------+--------------+
| 700 - 2000  | 0.04 - 6 dB/oct   | 10              | 120          |
+-------------+-------------------+-----------------+--------------+
| 2000        | 0.006             | 10              | 120          |
+-------------+-------------------+-----------------+--------------+

Test sequence
=============

+---------------------------------------------------------+
| Z test sequence                                         |
+=========+============================+==================+
| Test ID | Test Name                  | Profile          |
+---------+----------------------------+------------------+
| 1       | Resonance Survey MGSE Z    | Resonance Survey |
+---------+----------------------------+------------------+
| 2       | Initial Resonance Survey Z | Resonance Survey |
+---------+----------------------------+------------------+
| 3       | Sine Vibration Z           | Sine Vibration   |
+---------+----------------------------+------------------+
| 4       | Quasi Static Shock Z       | Sine Shock       |
+---------+----------------------------+------------------+
| 5       | Random Vibration Z         | Random Vibration |
+---------+----------------------------+------------------+
| 6       | Final Resonance Survey Z   | Resonance Survey |
+---------+----------------------------+------------------+

+---------------------------------------------------------+
| X/Y Test sequence                                       |
+=========+============================+==================+
| Test ID | Test Name                  | Profile          |
+---------+----------------------------+------------------+
| 7       | Resonance Survey MGSE Y/X  |                  |
+---------+----------------------------+------------------+

+---------------------------------------------------------+
| Y AXIS                                                  |
+=========+============================+==================+
| Test ID | Test Name                  | Profile          |
+---------+----------------------------+------------------+
| 8       | Initial Resonance Survey Y | Resonance Survey |
+---------+----------------------------+------------------+
| 9       | Sine Vibration Y           | Sine Vibration   |
+---------+----------------------------+------------------+
| 10      | Quasi Static Shock Y       |  Sine shock      |
+---------+----------------------------+------------------+
| 11      | Random Vibration Y         | Random Vibration |
+---------+----------------------------+------------------+
| 12      | Final Resonance Survey Y   | Resonance Survey |
+---------+----------------------------+------------------+

+---------------------------------------------------------+
| X AXIS                                                  |
+=========+============================+==================+
| Test ID | Test Name                  | Profile          |
+---------+----------------------------+------------------+
| 8       | Initial Resonance Survey X | Resonance Survey |
+---------+----------------------------+------------------+
| 9       | Sine Vibration X           | Sine Vibration   |
+---------+----------------------------+------------------+
| 10      | Quasi Static Shock X       |  Sine shock      |
+---------+----------------------------+------------------+
| 11      | Random Vibration X         | Random Vibration |
+---------+----------------------------+------------------+
| 12      | Final Resonance Survey X   | Resonance Survey |
+---------+----------------------------+------------------+

Expected Result:

#. The first modal frequency in each axis should be greater than 200Hz
#. The difference in the first modal frequency between the pre- and post-resonance survey should be under 5%
#. Visual inspection of the device after vibration should not show any signs of damage
#. Functional tests should be passed successfully before and after the vibration test

Bake - Out procudure
====================

Perform the Bake-out procedure for the Satellite as specified in `LSP-REQ-317_01A <https://www.nasa.gov/pdf/627972main_LSP-REQ-317_01A.pdf>`_:

* For proto-flight testing, Satellite should be baked in :math:`\\10^{-4}` Torr and 70°C for three hours after the temperature stabilization
* Satellite should be switched off (kill switches pressed) throughout the procedure
* Satellite should be weighed before and after the procedure, and the difference in mass should be less than 1% for the test to be successful
* Battery voltage should be measured before and after the test, and the difference should be less than 1%
* Satellites should be able to pass the functional tests after the bake-out test successfully

Functional testing
==================

PREREQUISITES:
The Satellite must be as it is, in the deployer, ready to be in orbit. This means that:

* It is in stowed configuration
* Identify which Satellite you are testing and mark its TX and RX frequencies as well as the SCID
* The firmware must be the final flight firmware
* The batteries must be fully charged
* The ground station (or ground support equipment) must be ready to communicate with the Satellite, and the logging system to be ready to record
* A 500W lamp and the tripod are ready to operate as a solar illuminator or "sun"
* A thermal camera is ready to record the temperatures while the satellite is under the "sun" and while the thermal knives are working

Ground Station test setup
=========================

Setup the ground station by following the instructions in the `Link <https://gitlab.com/librespacefoundation/qubik/qubik-listener#docker>`_. The default setup is for pluto SDR and the satellite flashed with Qubik 1 firmware, different setups can be found under the “docker-env_files” folder.

OVERVIEW:

* Start satellite operations in a state similar to being ejected on orbit
* Use ground station or ground support equipment to communicate, upload commands, and download data
* Command satellite to perform common operations
* Run a typical payload collection scenario and download the data. Confirm it is valid
* Use a solar illuminator if possible (or a charging source that is cycled in accordance with the expected orbit day/night cycle) to simulate  in-orbit battery charge/discharge
* Allow the test to run as long as possible (several days to a week)

Time Sequence:

+----------------+---------------------------------------------------------------------------------------------------------------------------------+
| Time           | State                                                                                                                           |
+================+=================================================================================================================================+
| T-hold of time | Activate satellite. Start counting down for the hold of time to start normal operation                                          |
+----------------+---------------------------------------------------------------------------------------------------------------------------------+
| T-2min         | Start thermal camera recording of thermal knifes                                                                                |
+----------------+---------------------------------------------------------------------------------------------------------------------------------+
| T+0            | Antenna release                                                                                                                 |
+----------------+---------------------------------------------------------------------------------------------------------------------------------+
| T+5min         | OSDLP operation of Satellite                                                                                                    |
+----------------+---------------------------------------------------------------------------------------------------------------------------------+
| T+10min        | Light up one panel each time for one minute and check the corresponding current value in telemetry. Iterate for all five panels |
+----------------+---------------------------------------------------------------------------------------------------------------------------------+
| T+15min        | End of test                                                                                                                     |
+----------------+---------------------------------------------------------------------------------------------------------------------------------+

Simple telemetry request:

#. Select :code:`2 | Request TM`
#. Select :code:`3 | Initiate with Set V(R) (Sends a command. Good for ping)` and set :code:`V(R)` to 0
#. Select :code:`r | [osdlp] Resume`. Until a proper frame arrives, the rest of the commands may not appear. If this is not happening, return to 1 or check for the lock flag
#. Select :code:`1 | Request TM`. Telemetry should arrive, and the sequence number should be increasing
#. Repeat step 4 multiple times. Check that the sequence number increases
#. Select :code:`2 | Request Manifesto` and make sure that the Manifesto arrives
#. Terminate the session for the VCID 2 using the :code:`2 | t | [osdlp] Terminate local osdlp service (Reset)`
#. Return to the VCID selection menu by selecting :code:`0 | Return to the VCID selection menu`

Simple Management:

#. Select :code:`0 | management`
#. If the Lock flag is not set, select :code:`3 | Initiate with Set V(R) (Sends a command. Good for ping)` and set the :code:`V(R)` to 0. Otherwise, initiate an unlock first
#. Select :code:`r | [osdlp] Resume`.Commands should be available only if step 2 were successful
#. Select :code:`2 | Request periodic telemetry attributes`.  You should receive the corresponding frame, and the sequence number should be increasing
#. Repeat step 4 multiple times. Check that the sequence number increases
#. Terminate the session for the VCID 2 using the :code:`t | [osdlp] Terminate local osdlp service (Reset)`
#. Return to the VCID selection menu by selecting :code:`0 | Return to the VCID selection menu.`

Test the locking and unlocking procedure:

For this test, we will lock the satellite by force and then unlock it. For this particular example, the locking will be done on the Request TM VCID 2, but it can also be tested on the Management VCID 0.

#. Select :code:`2 | Request TM`
#. If the Lock flag is not set, :code:`select 3 | Initiate with Set V(R) (Sends a command. Good for ping)` and set the :code:`V(R)` to 0. Otherwise, initiate an unlock first
#. Select :code:`r | [osdlp] Resume`. Commands should be available only if step 2 were successful
#. Select :code:`1 | Request TM`. Telemetry should arrive, and the sequence number should increase
#. Repeat step 4 multiple times until the sequence number reaches a number > 10 (the window that the OSDLP allows for out-of-order frames)
#. Kill the OSDLP session. Note that you have to close and restart the :code:`netcat`
#. Re-open OSDLP and select the same VCID used in step 1. For this example, the :code:`2 | Request TM`
#. Select :code:`2 | Initiate no CLCW (Don't expect anything. Begin TX)` . This will use the sequence number 0, because we just started again the osdp-operator
#. Select :code:`1 | Request TM`
#. From now on, the VC 2 should be in Lockout mode, and the corresponding flag should be true
#. Select :code:`r | [osdlp] Resume`. With the VC locked, this should return you to the initialization option menu
#. To start the unlock procedure, select the :code:`4 | Initiate with Unlock. (Another command. Also good for ping)`
#. Check the telemetry and identify that the lockout flag is set to false, otherwise, go to step 12
#. With the VC unlock, normally, the :code:`s | [osdlp] Set new V(S) (Local frame sequence number)` would be sufficient. However, due to a bug in the osdlp-operator this is not yet possible. For this reason, terminate the session using the t | [osdlp] Terminate local osdlp service (Reset)
#. Select :code:`3 | Initiate with Set V(R) (Sends a command. Good for ping)`` and set a new :code:`V(R)`, eg. 0
#. Select :code:`r | [osdlp] Resume`. If everything went well, the available MAP options should appear
#. Select :code:`1 | Request TM` and check if you receive the requested frame
#. Repeat 17 and check that everything performs well
#. Select :code:`t | [osdlp] Terminate local osdlp service (Reset)`

Change TRX delay:

#. Select :code:`0 | Management`
#. If the Lock flag is not set, select :code:`3 | Initiate with Set V(R) (Sends a command. Good for ping)` and set the :code:`V(R)` to 0. Otherwise, initiate an unlock first
#. Select :code:`r | [osdlp] Resume`. Commands should be available only if step 2 were successful.
#. Select :code:`5 | Set TRX delay`
#. The new TRX delay should be available on the telemetry. Until this is available through Grafana, the last 10 bytes of the telemetry hex output should start with :code:`0x00 0x64`

Day In The Life (DITL) Testing
==============================

This test, as described in `Cubesat
101 <https://www.nasa.gov/sites/default/files/atoms/files/nasa_csli_cubesat_101_508.pdf>`__,
validates that satellite software is nominally functional, and that the
combination of hardware and software can perform its basic mission. The
operation of satellite is documented and the same operation is expected
when the satellite is in the orbit.

**Time Sequence**

+-----------+-------------------------------------------------------------------------------------------------------------------------+
| Time      | State                                                                                                                   |
+===========+=========================================================================================================================+
| T+0       | Start counting down for 30 minutes to start normal operation                                                            |
+-----------+-------------------------------------------------------------------------------------------------------------------------+
| T+28min   | Prepare the camera to record the antenna deployment, prepare the thermal camera to evaluate the thermal knife operation |
+-----------+-------------------------------------------------------------------------------------------------------------------------+
| T+30min   | Antenna release                                                                                                         |
+-----------+-------------------------------------------------------------------------------------------------------------------------+
| T+1h      | 5min OSDLP operation or Ground station testing of Qubiks 2min30sec each from one station                                |
+-----------+-------------------------------------------------------------------------------------------------------------------------+
| T+1h5min  | 5min under lamp 1min per panel                                                                                          |
+-----------+-------------------------------------------------------------------------------------------------------------------------+
| T+2h      | 5min OSDLP operation or Ground station testing of Qubiks using 2 stations                                               |
+-----------+-------------------------------------------------------------------------------------------------------------------------+
| T+2h5min  | 5min under lamp 1min per panel                                                                                          |
+-----------+-------------------------------------------------------------------------------------------------------------------------+
| T+3h      | 5min OSDLP operation or Ground station testing of Qubiks using 2 stations                                               |
+-----------+-------------------------------------------------------------------------------------------------------------------------+
| T+3h5min  | 5min under lamp 1min per panel                                                                                          |
+-----------+-------------------------------------------------------------------------------------------------------------------------+
| T+4h      | 5min OSDLP operation or Ground station testingof Qubiks using 2 stations                                                |
+-----------+-------------------------------------------------------------------------------------------------------------------------+
| T+4h5min  | 5min under lamp 1min per panel                                                                                          |
+-----------+-------------------------------------------------------------------------------------------------------------------------+
| T+4h10min | End of test                                                                                                             |
+-----------+-------------------------------------------------------------------------------------------------------------------------+

The start/stop of test must be logged.
For each step follow previous sections.
