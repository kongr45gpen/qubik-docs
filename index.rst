QUBIK Mission and Bus documentation
###################################

.. toctree::
   :maxdepth: 2
   :caption: Qubik Mission
   :hidden:

   introduction
   objectives
   experiments

.. toctree::
   :maxdepth: 2
   :caption: Qubik Ground Station 
   :hidden:

   reception
   command

.. toctree::
   :maxdepth: 2
   :caption: Qubik Budgets 
   :hidden:

   budget

.. toctree::
   :maxdepth: 2
   :caption: Qubik Bus Components
   :hidden:

   comms-obc
   structural
   battery-management-power-system
   solar-power-board
   cabling

.. toctree::
   :maxdepth: 2
   :caption: Qubik Bus Assembly
   :hidden:

   pre-assembly
   Assembly Guide <https://qubik.libre.space/projects/qubik-assembly-guide>

.. toctree::
   :maxdepth: 2
   :caption: Qubik ICD
   :hidden:

   user-manual
   interface

.. toctree::
   :maxdepth: 2
   :caption: Qubik Testing
   :hidden:

   environmental-functional-testing

* :ref:`search`
