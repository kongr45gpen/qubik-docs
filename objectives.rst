############
Objectives
############

The objectives of the QUBIK mission can be categorized in two categories:

Satellite Identification and Tracking
-------------------------------------


GOAL1 : Unambiguously identify satellites as soon as possible after deployment
==============================================================================
Launch and early operations phase is a time critical and time sensitive period for a satellite after deployment. It is important to establish communication with the satellite quickly after launch, to address possible issues (Communications or Attitude related) and start payload commissioning. To reliably communicate with the satellite, positive identification is crucial. The QUBIK satellites are designed to allow for easy RF identification during Launch and Early Operations Phase (LEOP). It is worth noting that with many more satellites using very low earth orbits lately (for debris mitigation and cost reasons) maximizing the available payload mission time is critical 1.

GOAL2 : Generate or update existing orbital elements based on Doppler curve tracking of satellite transmissions
===============================================================================================================
Nano and micro-satellite operators are increasingly relying solely on external tracking of their satellite for a variety of reasons (saving on Attitude Determination and Control Systems - ADCS and operational expenses, power budget or COMMS budget). The only reliable available public resource for orbital elements has historically been the Combined Space Operations Center (CSpOC) (though their space-track.org2 dissemination website). With many nano and micro-satellites sharing the same launch vehicle, many are deployed together in the same orbit. The small radar cross section and crowding complicates tracking and identification by CSpOC, and it is not uncommon for initial orbital elements to be published with delays of up to a few weeks. Additionally, newly launched satellites on crowded launched are regularly mis-indentified, or not identified at all. For the QUBIK satellites, we will employ passive Doppler tracking through a global network of ground stations to independently determine orbital elements during LEOP.      

GOAL3 : Explore the above goals in a scalable, distributed and open source, open data way consistent with the Libre Space Manifesto.
====================================================================================================================================
Libre Space Foundation vowes to explore the above mentioned GOAL 1 and GOAL 2 in a way compatible with Libre Space Manifesto3. That essentially means that any technology developed (software, hardware) should be open source, and data captured should be readily available and distributed as open data, and any architecture deployed should be scalable and distributed.


Open Source Pocketqube Platform
-------------------------------

GOAL4 : Create a re-usable open source Pocketqube Platform
==========================================================
Pocketqube as a format can be a versatile low-cost format for a satellite. Libre Space Foundation believes that by creating a complete open source stack of technologies around the Pocketqube format we can enable a wide variety of experiments, paylaods, technology development and missions built at the state-of-the-art and not re-inventing the wheel. QUBIK serves as the realization of this vision.