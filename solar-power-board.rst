Solar Power Board
#################
* Description
* Repository
* Releases

System Perfomance
*****************
Related issues:

* https://gitlab.com/librespacefoundation/qubik/qubik-spb/-/issues/23
* https://gitlab.com/librespacefoundation/qubik/qubik-spb/-/issues/14

System Assembly
***************
Related issue:

* https://gitlab.com/librespacefoundation/qubik/qubik-docs/-/issues/12 and linked issues
* Before Gluing and coating a :doc:`dry fit <pre-assembly>` must be done

System Testing
**************
Related documentation:

* https://gitlab.com/librespacefoundation/qubik/qubik-spb/-/wikis/home
